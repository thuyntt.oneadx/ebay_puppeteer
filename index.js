const Hidemyacc = require("./hidemyacc");
const hidemyacc = new Hidemyacc();
const puppeteer = require("puppeteer-core");
const delay = (timeout) =>
  new Promise((resolve) => setTimeout(resolve, timeout));
const fs = require("fs"); //su dung thu vien fs de doc ghi file
const { log } = require("console");
const axios = require("axios");
!(async () => {

  const result = await ProfileID();
  const page = result.Page;
  const browser = result.Browser;
  const profileId = result.profileID;
  await page.click(
    'a[href="https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&ru=https%3A%2F%2Fwww.ebay.com%2F"]'
  );
  await delay(1000);
  //////////////////////////////////////////////////////
  //Login Ebay
  // const username = "aoppxohaupon@hldrive.com";
  // const password = "$H2qw1,7";

  const username = "rbkkcnrerwe@hldrive.com";
  const password = "TZD.@U5}";

  const resultLogin = await login(page, username, password);
  console.log(resultLogin.msg);

  if (resultLogin.success === true) {
    fs.appendFileSync(
      "loginSuccess.txt",
      resultLogin.msg + " voi tai khoan: " + username + "\n"
    );
    await findProduct(browser, page);
    await delay(3000);
    // await hidemyacc.stop(profileId);
  } else {
    fs.appendFileSync(
      "loginFalse.txt",
      resultLogin.msg + " voi tai khoan: " + username + "\n"
    );
    await delay(3000);
    await hidemyacc.stop(profileId);
    // await hidemyacc.delete(profileId);
  }
})();

//Create,stop,delete ProfileID
async function ProfileID() {
  try {
    //create ProfileID
    let profile = {
      id: "",
      name: "EbayExample",
      os: "win",
      platform: "Win32",
      browserSource: "ghosty",
      browserType: "chrome",
      proxy: {
        proxyEnabled: false,
        autoProxyServer: "",
        autoProxyUsername: "",
        autoProxyPassword: "",
        changeIpUrl: "",
        mode: "http",
        port: 80,
        autoProxyRegion: "VN",
        torProxyRegion: "us",
        host: "",
        username: "",
        password: "",
      },
    };
    let user = await hidemyacc.create(profile);
    let profileId = await user.data.id;
    const response = await hidemyacc.start(profileId);
    if (response.code !== 1) {
      throw new Error("Khong mo duoc trinh duyet");
    }

    const browser = await puppeteer.connect({
      browserWSEndpoint: response.data.wsUrl,
      defaultViewport: null,
      slowMo: 60,
    });

    const pages = await browser.pages();
    let page;

    if (pages.length) {
      page = pages[0];
    } else {
      page = await browser.newPage();
    }
    await page.goto("https://www.ebay.com/", {
      waitUntil: "networkidle2",
      timeout: 5000,
    });

    return { profileID: profileId, Page: page, Browser: browser };
  } catch (e) {
    console.log(e.message);
  }
}

//Login Ebay
async function login(page, username, password) {
  try {
    const response = {
      success: false,
      msg: "",
    };

    const inputUsername = await page.$("input#userid");
    let currentUrl = await page.url();
    if (inputUsername && currentUrl.includes("signin.ebay.com/ws")) {
      await page.type('input[name="userid"]', username);
      await delay(1000);
      await page.keyboard.press("Enter");
      await delay(1000);
      currentUrl = await page.url();
      const error = await page.$("div.inline-notice");
      if (error) {
        response.success = false;
        response.msg = "Loi nhap sai ten dang nhap";
        return response;
      }
    } else if (currentUrl.includes("/splashui/captcha")) {
      response.success = false;
      response.msg = "Loi captcha";
      return response;
    } else if (currentUrl.includes("/logout/confirm")) {
      await page.click("a#signin-link");
      currentUrl = await page.url();
      return login(page, username, password);
    } else {
      response.success = true;
      response.msg = "Dang nhap thanh cong";
      return response;
    }
    //Lay ra gia tri cua username sau khi da nhap username thanh cong
    const inputPassword = await page.$eval("span#user-info", (element) => {
      return element.textContent;
    });
    if (inputPassword == username) {
      await page.type('input[type="password"]', password);
      await delay(1000);
      await page.keyboard.press("Enter");
      await delay(3000);
      currentUrl = await page.url();
      const error = await page.$('div[aria-live="polite"]');
      if (error) {
        response.success = false;
        response.msg = "Loi nhap sai mat khau";
        return response;
      }
      return await login(page, username, password);
    }
    return response;
  } catch (e) {
    console.error(e.message);
  }
}

//Ham de lay key va value tu api
async function getKey(apiUrl) {
  try {
    const response = await axios.get(apiUrl);
    if (response.status === 200) {
      const responseData = response.data;
      // Lay danh sach keys tu du lieu API
      const keys = Object.keys(responseData);
      //Lay gia tri tuong ung voi moi key
      const values = keys.map((key) => responseData[key]);
      //Tra ve mot doi tuong chua keys va values
      return {
        keys: keys,
        values: values,
      };
    }
  } catch (error) {
    console.error("Loi khi gui yeu cau den API:", error);
    return {
      keys: [],
      values: [],
    };
  }
}

//Tim san pham
async function findProduct(browser, page,) {
  const apiUrl = "https://autosug.hidemyacc.com/keywords/ebay";
  let result = await getKey(apiUrl);
  const values = result.values;
  await page.type("input.gh-tb", values);
  await delay(3000);
  await page.keyboard.press("Enter");
  await delay(3000);
  let listTitles = await page.$$('span[role="heading"]');
  await getLink(1, listTitles, page, browser);
}

//Lay link cac san pham
async function getLink(countTitle, listTitles, page, browser) {
  try {
    let check = 1; //Bien ktra so lan tim kiem
    const totalSearch = 5; //Tong so lan tim kiem san pham
    while (true) {
      if (countTitle <= listTitles.length) {
        //countTitle: Bien dem so san pham trong mot page
        await listTitles[countTitle].click();
        await delay(3000);
        //Lay ra cac tab hien tai
        page = await browser.pages();
        await page[1].click("button.fake-link");
        await delay(1000);
        const link = await page[1].$eval("div.text-content", (element) => {
          return element.textContent;
        });
        fs.appendFileSync("linkProduct.txt", link + ' " \n');
        countTitle++;
        await page[1].close();
        await page[0].bringToFront();
      } 
      else {
        page = await browser.pages();
        //Lay gia tri cua nut next page
        const nextPageButton = await page[0].$(".pagination__next");
        //Kiem tra xem nut next page co ton tai hay khong
        const nextButton = await page[0].$(
          '.pagination__next[aria-disabled="true"]'
        );
        if (nextPageButton && !nextButton) {
          await nextPageButton.click();
          await delay(1000);
          page = await browser.pages();
          countTitle = 1;
          listTitles = await page[0].$$('span[role="heading"]');
          return getLink(countTitle, listTitles, page, browser);
        }else if(check >= totalSearch){
          console.log("Het so lan tim kiem");
          break;
        } 
        else {
          page = await browser.pages();
          await page[0].click("a#gh-la");
          await delay(1000);
          console.log("check: " + check);
          check++;
          await findProduct(browser, page[0]);
        }
      } 
    }
  } catch (e) {
    console.error(e.message);
  }
}
